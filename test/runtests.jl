using Anorde
using Test
using Anorde.SlidingWindows
using Statistics

@testset "Sliding Window" begin
	data = rand(rand(30:40))
	ws = SlidingWindows.SlidingWindow(data, 3)
	@test all(mean.(ws) .≈ [mean(data[(i-2):i]) for i in 3:lastindex(data)])
	@test all(maximum.(ws) .≈ [maximum(data[(i-2):i]) for i in 3:lastindex(data)])
end

