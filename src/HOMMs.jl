module HOMMs

using ..DataHelper

struct HOMM
	order::Integer
	states::AbstractArray
	labels::AbstractArray
	transitions::AbstractArray
	data::AbstractArray
	dist::Dict

	function HOMM(data::AbstractArray
					  , order::Integer
					  , labels::AbstractArray
					  , states::AbstractArray
					  ; maxd::Number = maximum(data)
					  , mind::Number = minimum(data))
		nstate = length(states)
		transitions = map(1:order) do n
			M = Mn(labels, states, order)
			return M ./ sum(M; dims = 2)
		end
		transitions = replace(reshape((collect ∘ Iterators.flatten)(transitions)
												, nstate
												, nstate
												, order)
									 , NaN => 0)
		dist = Dict(i => count(labels .== i) for i in eachindex(states))
		return new(order, states, labels, transitions, data, dist)
	end
	

	function HOMM(data::AbstractArray, order::Integer, nstate::Integer)
		labels, states = state_divide(data, nstate)
		HOMM(data, order, labels, states)
	end
end

"""
	Mn(labels::AbstractArray,states::AbstractArray,n::Integer)

Count the number of state transitions from the array of labels

...
# Arguments
- `labels::AbstractArray`: 
- `states::AbstractArray`: 
- `n::Integer`: number of step
"""
function Mn(labels::AbstractArray, states::AbstractArray, n::Integer)
    N = length(states)
    M = zeros(Int, N, N)
	 for ind in CartesianIndex.(labels[1:(end-n)], labels[(n + 1):end])
        M[ind] = M[ind] + 1
    end
    return M
end

"""
isanomaly(m::DMM, label::Integer)

Check if the `i`-th data point is abnormal

...
# Arguments
- `m::DMM`: the model
- `label::Integer`: the label index
"""
function isanomaly(m::HOMM, label::Integer)
	prob = sum(m.dist[j] * m.transitions[j, label, i] for i in 1:m.order, j in eachindex(m.states))
	return prob == zero(prob)
end

function expand_states(m::HOMM, x::Number)
	local mxsr, mxidx = maximum(m.states)
	local mnsr, mnidx = minimum(m.states)
	new_states = if x > mxsr
		expand(m.states[mxidx], x, :up)
	elseif x < mnsr
		expand(m.states[mnidx], x, :down)
	end
	return HOMM(m.data, m.order, m.labels, [m.states; new_states])
end


export HOMM, isanomaly, expand_states
end
