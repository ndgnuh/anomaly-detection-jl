"""
Basic actions for Variable Order Makov model
"""
module DMMs

using LinearAlgebra
using DataStructures
using ProgressMeter
using Statistics
using ..SlidingWindows
using ..DataHelper
using ..SubstitutionStrategies

export DMM, DMMLabels, train!, append!, Mn, isanomaly, detect

"""
Variable Markov Model (Rewrite), use Signal to reactively update states
"""
function DMMold(data::AbstractArray, wl::Integer, ns::Union{AbstractArray, Integer};
					 subs = Nothing, mincor = 0.8, maxbound = Inf, minbound = -Inf)
	w = SlidingWindow(copy(data), wl)
	lenw = length(w)
	orders = fill(1, length(w))
	# detection result
	result = fill(false, length(data))
	deleteidx = fill(0, length(w.data))
	# current window
	for cw = 1:(lenw - 1)
		if cw > lenw break end
		# find max order
		order = 1
		for n = 1:wl
			if cw + n > lenw break end
			corr = cor(w[cw], w[cw + n])
			if corr < mincor
				break
			else
				order = n
				orders[cw] = order
			end
		end
		# if there's a correlated window
		# then that window must not be anomaly
		cw_data = w[cw]

		# divide in the state ranges
		if cw + w.len > length(w.data)
			break
		end
		nextpoint = w.data[cw + w.len]
		labels, states = state_divide([cw_data; nextpoint], ns)
		# initial distribution
		dist = counter(labels[1:(end-1)])
		# transition matrices
		trans = transitions(labels[1:end-1], ns, order)
		# detect anomaly from s(t-i) -> st
		st = labels[end]
		prob = dist[labels[end - order]] * prod(trans[labels[end - i], st, i] for i = 1:order)
		result[cw + w.len] = iszero(prob) || nextpoint > maxbound || nextpoint < minbound


		# substitution, new code with kernel
		if result[cw + w.len] && subs != Nothing
			candidates_probs = zeros(length(states))
			for st′ in eachindex(states)
				# display(trans)
				candidates_probs[st′] = sum(trans[labels[end - i + 1], st′, i] for i = 1:order)
			end
			maxprob, sub_idx = findmax(candidates_probs)
			_, oldstates = state_divide([cw_data; nextpoint], ns)
			
			if maxprob ≈ 0 @error "No substitution, sub prob $maxprob" end
			
			# substitute(::Type, wt, xt, states, oldstates, labels, st_idx)
			if subs == SubstitutionStrategies.Skip
				deleteat!(w.data, cw + w.len)
				lenw = lenw - 1
			else
				w.data[cw + w.len] = SubstitutionStrategies.substitute(subs,
																						 w[cw],
																						 w.data[cw + w.len],
																						 states,
																						 oldstates,
																						 labels[1:end-1],
																						 sub_idx,
																						 order)
			end
		end


		# substitutions
		# if subs != Nothing && iszero(prob)
		# 	candidates_probs = zeros(length(states))
		# 	for st′ in eachindex(states)
		# 		# display(trans)
		# 		candidates_probs[st′] = sum(trans[labels[end - i], st′, i] for i = 1:order)
		# 	end
		# 	# substitue
		# 	maxprob, sub_idx = findmax(candidates_probs)
		# 	if maxprob ≈ 0
		# 		display(trans)
		# 		@error "No substitution"
		# 	end
		# 	prev_data = w.data[cw + w.len - 1]

		# 	sub_datum = lastindex(labels) - 1
		# 	st_new = states[sub_idx]
		# 	while sub_datum > 0
		# 		if labels[sub_datum] == sub_idx
		# 			break
		# 		end
		# 		sub_datum -= 1
		# 	end
		# 	if prev_data < w.data[cw + w.len]
		# 		w.data[cw + w.len] = mean([cw_data[sub_datum], prev_data, st_new.max])
		# 	else
		# 		w.data[cw + w.len] = mean([cw_data[sub_datum], prev_data, st_new.min])
		# 	end
		# end
	end
	return result, w.data, orders
end

function Mn(labels::AbstractArray, nstate::Integer, n::Integer)
    M = zeros(Int, nstate, nstate)
	 for ind in CartesianIndex.(labels[1:(end-n)], labels[(n + 1):end])
        M[ind] = M[ind] + 1
    end
	 return M
end

function transitions(labels::AbstractArray
							, states::AbstractArray
							, order::Integer)
	return transitions(labels, length(states), order)
end

function transitions(labels::AbstractArray
							, nstate::Integer
							, order::Integer)
	transitions = map(1:order) do n
		M = Mn(labels, nstate, n)
		for i = 1:size(M, 1)
			if sum(M[i, :]) == 0
				M[i, i] = 1
			end
		end
		# if flag; display(M); end
		M ./ sum(M; dims = 2)
	end
	transitions = (collect ∘ Iterators.flatten)(transitions)
	transitions = reshape(transitions, nstate, nstate, order)
	transitions
end

mutable struct DMM
	data::AbstractArray
	wl::Integer
	ns::Integer
	orders::AbstractArray
	transitions::AbstractArray
	states::AbstractArray
	dists

	function DMM(xs::AbstractArray, wl::Integer, ns::Union{AbstractArray, Integer}; mincor = 0.8)
		T = length(xs)
		wls = map(wl + 1:T) do t
			wt = xs[t - wl:t - 1]
			ls, ss = state_divide(wt, ns)
			ls, ss, counter(ls)
		end

		orders = map(wl + 1:T) do t
			order = 1
			wt = xs[t - wl:t - 1]
			for n = 1:T - t + 1
				t′ = t + n
				if t′ > T break end
				wtpn = xs[t′ - wl:t′ - 1]
				if cor(wt, wtpn) < mincor
					return order
				end
				order = n
			end
			order
		end

		wlabels = getindex.(wls, 1)
		wstates = getindex.(wls, 2)
		wdists = getindex.(wls, 3)

		trans = map(1:T - wl) do t
			transitions(wlabels[t], wstates[t], orders[t])
		end
		
		nstate = typeof(ns) <: Integer ? ns : length(ns) 

		return new(xs, wl, nstate, orders
					  , trans, wstates , wdists)
	end
end


function fit!(m::DMM, xs::AbstractArray; subs = Nothing)
	# copy so as not to mutate the data
	xs = copy(xs)
	wl = m.wl
	ns = m.ns
	T = length(xs)
	newT = T
	ntrain = length(m.orders)
	results = fill(false, T)
	# main loop
	for t = wl + 1:length(xs)
		if t - wl < length(m.data)
			continue
		end
		if newT < t break end
		cw = (t - wl - 1) % ntrain + 1
		wt = xs[wind(wl, t)]
		n = m.orders[cw]
		states = m.states[cw]
		dist = m.dists[cw]
		trans = m.transitions[cw]

		# label the states
		labels = map(wt) do x
			which_state(states, x)
		end
		# if can't label the new data with old states
		if any(isnothing.(labels))
			wt_train = m.data[cw:cw + m.wl - 1]

			# redefine states
			newlabels, m.states[cw] = state_divide([wt_train; wt], m.ns)
			states = m.states[cw]
			dist = m.dists[cw] = counter(newlabels)

			# recalculate transitions
			train_labels = newlabels[1:m.wl]
			labels = newlabels[m.wl + 1:end]

			# m.transitions[cw] = transitions(train_labels, m.ns, n)
			trans = m.transitions[cw] = transitions(newlabels, m.ns, n)
			# assign test labels
			# labels = newlabels[wl + 2:end]
			# st = newlabels[1]
		end

		# find label of next point
		st = which_state(states, xs[t])
		oldstates = copy(states)
		oldlabels = copy(labels)
		if isnothing(st)
			labels, states = state_divide([wt; xs[t]], m.ns)
			st = labels[end]
			labels = labels[1:end - 1]
			trans = m.transitions[cw] = transitions(labels, m.ns, n)
			dist = m.dists[cw] = counter(labels)
		end

		# support prob
		prob = dist[labels[end - n + 1]] * prod(map(1:n) do i; trans[labels[end - i], st, i]; end)
		results[t - wl] = iszero(prob)

		# substitute
		if results[t - wl] && subs != Nothing
			candidates_probs = zeros(length(states))
			for st′ in eachindex(states)
				# display(trans)
				candidates_probs[st′] = sum(trans[labels[end - i + 1], st′, i] for i = 1:n)
			end
			maxprob, sub_idx = findmax(candidates_probs)
			
			if maxprob ≈ 0 @error "No substitution, sub prob $maxprob" end
			
			# substitute(::Type, wt, xt, states, oldstates, labels, st_idx)
			if subs == SubstitutionStrategies.Skip
				deleteat!(xs, t)
				newT = newT - 1
			else
				xs[t] = SubstitutionStrategies.substitute(subs, wt, xs[t], states, oldstates, labels, sub_idx, n)
			end
		end
	end
	return results, xs
end

function wind(wl::Integer, t::Integer)
	t - wl:t -1
end


# end of module
end
