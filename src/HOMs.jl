module HOMs

using ..DataHelper

struct HOM
	order::Integer
	states::AbstractArray
	labels::AbstractArray
	transitions::AbstractArray

	function HOM(data::AbstractArray, order::Integer)
		labels, states = state_divide(data)
		transitions = map(1:order) do n
			M = Mn(labels, states, n)
			return M ./ sum(M; dims = 2)
		end
		transitions = replace(reshape((collect ∘ Iterators.flatten)(transitions)
												, nstate
												, nstate
												, order)
									 , NaN => 0)
		return new(order, states, labels, transitions)
	end
end

"""
	Mn(labels::AbstractArray,states::AbstractArray,n::Integer)

Count the number of state transitions from the array of labels

...
# Arguments
- `labels::AbstractArray`: 
- `states::AbstractArray`: 
- `n::Integer`: 
"""
function Mn(labels::AbstractArray, states::AbstractArray, n::Integer)
    N = length(states)
    M = zeros(Int, N, N)
	 for ind in CartesianIndex.(labels[1:(end-n)], labels[(n + 1):end])
        M[ind] = M[ind] + 1
    end
    return M
end

export HOM
end
